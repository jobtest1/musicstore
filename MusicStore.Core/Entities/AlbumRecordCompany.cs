﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class AlbumRecordCompany : BaseEntity
    {
        public int AlbumRecordCompany1 { get; set; }
        public int? AlbumId { get; set; }
        public int? RecordCompanyId { get; set; }

        public virtual Album Album { get; set; }
        public virtual RecordCompany RecordCompany { get; set; }
    }
}
