using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MusicStore.Api.Controllers;
using MusicStore.Core.Interfaces;
using System;
using Xunit;

namespace MusicStore.UnitTest
{
    public class MusicControllerTest
    {
        MusicController _controller;
        ISongService _songService;
        IAlbumService _albumService;
        IProductorService _productorService;
        IReviwerService _reviwerService;
        ILoggerManager _logger;
        IMapper _mapper;

        public MusicControllerTest(ISongService songService,
                               IAlbumService albumService,
                               IProductorService productorService,
                               IReviwerService reviwerService,
                               ILoggerManager logger,
                               IMapper mapper)
        {

            _songService = songService;
            _albumService = albumService;
            _productorService = productorService;
            _reviwerService = reviwerService;
            _logger = logger;
            _mapper = mapper;
            _controller = new MusicController(_songService, _albumService, _productorService, _reviwerService, _logger, _mapper);
        }

        [Fact]
        public void SongGet_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetSongsByAlbum("Album 1");
            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void SongGet_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetSongsByAlbum("Album 10");
            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void AlbumGet_WhenCalled_ReturnsOkResult()
        {
            // Act
            var okResult = _controller.GetSongsByAlbum("Album 3");
            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void AlbumGet_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Act
            var notFoundResult = _controller.GetAlbumByName("Album 10");
            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }
    }
}
