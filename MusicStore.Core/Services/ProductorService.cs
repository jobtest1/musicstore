﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicStore.Core.Services
{
    public class ProductorService : IProductorService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProductorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Productor>> GetByAlbum(string albumName)
        {
            try
            {
                var album = _unitOfWork.AlbumRepository.GetByName(albumName);

                if (album != null)
                {
                    var albumProductors = _unitOfWork.AlbumProductorRepository.GetAlbumProductorByAlbum(album.AlbumId);
                    List<Productor> productors = new List<Productor>();

                    foreach (var item in albumProductors)
                    {
                        var productor = await _unitOfWork.ProductorRepository.GetById((int)item.ProductorId);
                        productors.Add(productor);
                    }

                    return productors;
                }
                else
                    return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
