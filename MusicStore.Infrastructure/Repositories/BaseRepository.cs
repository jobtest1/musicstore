﻿using Microsoft.EntityFrameworkCore;
using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicStore.Infrastructure.Repositories
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MusicStoreContext _context;
        protected DbSet<T> _entities;
        public BaseRepository(MusicStoreContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            try
            {
                return _entities.AsEnumerable();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<T> GetById(int id)
        {
            try
            {
                return await _entities.FindAsync(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Add(T entity)
        {
            try
            {
                await _entities.AddAsync(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(T entity)
        {
            try
            {
                _entities.Update(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(int id)
        {
            try
            {
                T entity = await GetById(id);
                _entities.Remove(entity);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
