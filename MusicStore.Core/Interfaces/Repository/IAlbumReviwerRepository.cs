﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.Interfaces
{
    public interface IAlbumReviwerRepository : IRepository<AlbumReviwer>
    {
        List<AlbumReviwer> GetReviewsByAlbum(int albumId);
    }
}
