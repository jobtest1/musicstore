﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.UnitTest
{
    public class AlbumServiceFake : IAlbumService
    {
        private readonly FakeData _fakeData;

        public AlbumServiceFake()
        {
            _fakeData = new FakeData();
        }

        public Task<List<Album>> GetAlbumsByArtist(string artistName)
        {
            throw new NotImplementedException();
        }

        public Album GetByName(string name)
        {
            return _fakeData.Albums.FirstOrDefault(x => x.Name == name);
        }
    }
}
