﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.Interfaces
{
    public interface IAlbumProductorRepository : IRepository<AlbumProductor>
    {
        List<AlbumProductor> GetAlbumProductorByAlbum(int albumId);
    }
}
