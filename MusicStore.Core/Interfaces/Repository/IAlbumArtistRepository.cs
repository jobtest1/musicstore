﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.Interfaces
{
    public interface IAlbumArtistRepository : IRepository<AlbumArtist>
    {
        List<AlbumArtist> GetAlbumArtistByArtist(int artistId);
    }
}
