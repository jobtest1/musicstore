﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.DTOs
{
    public class ProductorDto
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
