﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.DTOs
{
    public class AlbumRatingDto
    {
        public int Rating { get; set; }
        public int ReviewsCount { get; set; }
    }
}
