﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.DTOs
{
    public class SongDto
    {
        //public int SongId { get; set; }
        //public int? AlbumId { get; set; }
        public int? TrackNumber { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public int? Popularity { get; set; }
        public decimal? SongPrice { get; set; }
        //public bool? IsEnabled { get; set; }
        //public bool? IsDeleted { get; set; }
    }
}
