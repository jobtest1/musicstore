﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Artist : BaseEntity
    {
        public Artist()
        {
            AlbumArtist = new HashSet<AlbumArtist>();
        }

        public int ArtistId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AlbumArtist> AlbumArtist { get; set; }
    }
}
