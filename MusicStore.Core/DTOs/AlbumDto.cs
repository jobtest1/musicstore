﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.DTOs
{
    public class AlbumDto
    {
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public DateTime? ReleasedDate { get; set; }
        public DateTime? RecordedDate { get; set; }
        public decimal? AlbumPrice { get; set; }
        public string MainReview { get; set; }
    }
}
