﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Mappings;
using MusicStore.Logger.Service;

namespace MusicStore.UnitTest
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ISongService, SongServiceFake>();
            services.AddTransient<IAlbumService, AlbumServiceFake>();
            services.AddTransient<IProductorService, ProductorServiceFake>();
            services.AddTransient<IReviwerService, ReviwerServiceFake>();

            services.AddAutoMapper(typeof(AutomapperProfile));
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }
    }
}
