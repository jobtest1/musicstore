﻿using Microsoft.EntityFrameworkCore;
using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class SongRepository : BaseRepository<Song>, ISongRepository
    {
        public SongRepository(MusicStoreContext context) : base(context){ }

        public List<Song> GetSongByAlbum(string albumName)
        {
            try
            {
                var songs = _entities
                .Include(i => i.Album)
                .Where(x => x.Album.Name == albumName)
                .ToList();

                return songs;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
