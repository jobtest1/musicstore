﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class AlbumReviwerRepository : BaseRepository<AlbumReviwer>, IAlbumReviwerRepository
    {
        public AlbumReviwerRepository(MusicStoreContext context) : base(context) { }

        public List<AlbumReviwer> GetReviewsByAlbum(int albumId)
        {
            try
            {
                var albumReviews = _entities.Where(x => x.AlbumId == albumId).ToList();
                return albumReviews;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
