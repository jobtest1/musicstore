﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ISongRepository SongRepository { get; }
        IAlbumRepository AlbumRepository { get; }
        IAlbumArtistRepository AlbumArtistRepository { get; }
        IArtistRepository ArtistRepository { get; }
        IAlbumProductorRepository AlbumProductorRepository { get; }
        IProductorRepository ProductorRepository { get; }
        IAlbumReviwerRepository AlbumReviwerRepository { get; }

        IRepository<Reviwer> ReviwerRepository { get;  }

        void SaveChanges();

        Task SaveChangesAsync();
    }
}
