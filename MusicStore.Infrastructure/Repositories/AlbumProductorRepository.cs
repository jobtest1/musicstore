﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class AlbumProductorRepository : BaseRepository<AlbumProductor>, IAlbumProductorRepository
    {
        public AlbumProductorRepository(MusicStoreContext context) : base(context) { }

        public List<AlbumProductor> GetAlbumProductorByAlbum(int albumId)
        {
            try
            {
                return _entities.Where(x => x.AlbumId == albumId).ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
