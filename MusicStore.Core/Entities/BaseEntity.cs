﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.Entities
{
    public abstract class BaseEntity
    {
        public bool? IsEnabled { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
