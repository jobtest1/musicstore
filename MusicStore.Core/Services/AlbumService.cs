﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicStore.Core.Services
{
    public class AlbumService : IAlbumService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AlbumService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<Album>> GetAlbumsByArtist(string artistName)
        {
            try
            {
                var artist = _unitOfWork.ArtistRepository.GetByName(artistName);

                if (artist != null)
                {
                    var albumArtists = _unitOfWork.AlbumArtistRepository.GetAlbumArtistByArtist(artist.ArtistId);
                    List<Album> albums = new List<Album>();
                    foreach (var item in albumArtists)
                    {
                        var album = await _unitOfWork.AlbumRepository.GetById((int)item.AlbumId);
                        albums.Add(album);
                    }
                    return albums;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Album GetByName(string name)
        {
            try
            {
                var album = _unitOfWork.AlbumRepository.GetByName(name);
                return album;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
