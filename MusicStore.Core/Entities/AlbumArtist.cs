﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class AlbumArtist : BaseEntity
    {
        public int AlbumArtistId { get; set; }
        public int? AlbumId { get; set; }
        public int? ArtistId { get; set; }

        public virtual Album Album { get; set; }
        public virtual Artist Artist { get; set; }
    }
}
