﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class AlbumGenre : BaseEntity
    {
        public int AlbumGenreId { get; set; }
        public int? AlbumId { get; set; }
        public int? GenreId { get; set; }

        public virtual Album Album { get; set; }
        public virtual Genre Genre { get; set; }
    }
}
