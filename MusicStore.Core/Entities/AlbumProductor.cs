﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class AlbumProductor : BaseEntity
    {
        public int AlbumProductorId { get; set; }
        public int? AlbumId { get; set; }
        public int? ProductorId { get; set; }

        public virtual Album Album { get; set; }
        public virtual Productor Productor { get; set; }
    }
}
