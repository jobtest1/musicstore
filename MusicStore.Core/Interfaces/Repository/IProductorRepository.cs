﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.Interfaces
{
    public interface IProductorRepository : IRepository<Productor>
    {
        Productor GetByName(string name);
    }
}
