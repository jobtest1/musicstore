﻿using MusicStore.Core.CustomEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface IReviwerService
    {
        Task<AlbumRating> GetAlbumRating(string albumName);
    }
}
