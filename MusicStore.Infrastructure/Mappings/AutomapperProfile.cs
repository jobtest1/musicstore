﻿using AutoMapper;
using MusicStore.Core.CustomEntities;
using MusicStore.Core.DTOs;
using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Infrastructure.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Song, SongDto>();
            CreateMap<SongDto, Song>();
            CreateMap<Album, AlbumDto>();
            CreateMap<AlbumDto, Album>();
            CreateMap<Productor, ProductorDto>();
            CreateMap<ProductorDto, Productor>();
            CreateMap<AlbumRating, AlbumRatingDto>();
            CreateMap<AlbumRatingDto, AlbumRating>();
        }
    }
}
