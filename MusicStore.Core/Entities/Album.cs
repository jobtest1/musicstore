﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Album : BaseEntity
    {
        public Album()
        {
            AlbumArtist = new HashSet<AlbumArtist>();
            AlbumGenre = new HashSet<AlbumGenre>();
            AlbumProductor = new HashSet<AlbumProductor>();
            AlbumRecordCompany = new HashSet<AlbumRecordCompany>();
            AlbumReviwer = new HashSet<AlbumReviwer>();
            Song = new HashSet<Song>();
        }

        public int AlbumId { get; set; }
        public string Name { get; set; }
        public string CoverImage { get; set; }
        public DateTime? ReleasedDate { get; set; }
        public DateTime? RecordedDate { get; set; }
        public decimal? AlbumPrice { get; set; }
        public string MainReview { get; set; }

        public virtual ICollection<AlbumArtist> AlbumArtist { get; set; }
        public virtual ICollection<AlbumGenre> AlbumGenre { get; set; }
        public virtual ICollection<AlbumProductor> AlbumProductor { get; set; }
        public virtual ICollection<AlbumRecordCompany> AlbumRecordCompany { get; set; }
        public virtual ICollection<AlbumReviwer> AlbumReviwer { get; set; }
        public virtual ICollection<Song> Song { get; set; }
    }
}
