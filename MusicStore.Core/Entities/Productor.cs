﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Productor : BaseEntity
    {
        public Productor()
        {
            AlbumProductor = new HashSet<AlbumProductor>();
        }

        public int ProductorId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }

        public virtual ICollection<AlbumProductor> AlbumProductor { get; set; }
    }
}
