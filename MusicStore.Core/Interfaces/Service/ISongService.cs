﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface ISongService
    {
        IEnumerable<Song> GetSongs();
        List<Song> GetSongByAlbum(string albumName);
        Task<Song> GetSong(int id);
        //Task InsertSong(Song song);
        //Task<bool> UpdateSong(Song song);
        //Task<bool> DeleteSong(int id);
    }
}
