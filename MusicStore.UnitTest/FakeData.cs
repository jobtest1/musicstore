﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.UnitTest
{
    public class FakeData
    {
        public List<Song> Songs { get; set; }
        public List<Album> Albums { get; set; }
        public List<Productor> Productors { get; set; }
        public List<Reviwer> Reviwers { get; set; }
        public List<AlbumReviwer> AlbumReviwers { get; set; }

        public FakeData()
        {
            Songs = new List<Song>()
            {
                new Song(){ SongId = 1, TrackNumber = 1 ,AlbumId = 1, Name = "Song A", Duration = "0:50", SongPrice = 1.20M},
                new Song(){ SongId = 2, TrackNumber = 2 ,AlbumId = 1, Name = "Song B", Duration = "0:50", SongPrice = 1.30M},
                new Song(){ SongId = 3, TrackNumber = 3 ,AlbumId = 2, Name = "Song C", Duration = "0:50", SongPrice = 1.50M},
                new Song(){ SongId = 4, TrackNumber = 4 ,AlbumId = 2, Name = "Song D", Duration = "0:50", SongPrice = 1.40M},
                new Song(){ SongId = 5, TrackNumber = 5 ,AlbumId = 2, Name = "Song E", Duration = "0:50", SongPrice = 1.90M},
                new Song(){ SongId = 6, TrackNumber = 6 ,AlbumId = 3, Name = "Song F", Duration = "0:50", SongPrice = 1.20M}
            };

            Albums = new List<Album>
            {
                new Album(){AlbumId = 1, Name = "Album 1"},
                new Album(){AlbumId = 2, Name = "Album 2"},
                new Album(){AlbumId = 3, Name = "Album 3"}
            };

            Productors = new List<Productor>
            {
                new Productor(){ProductorId = 1, Name = "Productor 1"},
                new Productor(){ProductorId = 2, Name = "Productor 2"}
            };

            Reviwers = new List<Reviwer>
            {
                new Reviwer(){ReviwerId = 1, Rating = 4},
                new Reviwer(){ReviwerId = 2, Rating = 3},
                new Reviwer(){ReviwerId = 3, Rating = 5}
            };

            AlbumReviwers = new List<AlbumReviwer>
            {
                new AlbumReviwer(){AlbumReviwerId = 1, AlbumId = 1, ReviwerId = 1},
                new AlbumReviwer(){AlbumReviwerId = 2, AlbumId = 1, ReviwerId = 2},
                new AlbumReviwer(){AlbumReviwerId = 3, AlbumId = 1, ReviwerId = 3},
            };
        }
    }
}
