﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class AlbumReviwer : BaseEntity
    {
        public int AlbumReviwerId { get; set; }
        public int? AlbumId { get; set; }
        public int? ReviwerId { get; set; }

        public virtual Album Album { get; set; }
        public virtual Reviwer Reviwer { get; set; }
    }
}
