﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicStore.Core.Services
{
    public class SongService : ISongService
    {
        private readonly IUnitOfWork _unitOfWork;

        public SongService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Song> GetSongs()
        {
            try
            {
                return _unitOfWork.SongRepository.GetAll();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<Song> GetSongByAlbum(string albumName)
        {
            try
            {
                return _unitOfWork.SongRepository.GetSongByAlbum(albumName);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<Song> GetSong(int id)
        {
            try
            {
                return await _unitOfWork.SongRepository.GetById(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
