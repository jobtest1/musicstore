﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class AlbumArtistRepository : BaseRepository<AlbumArtist>, IAlbumArtistRepository
    {
        public AlbumArtistRepository(MusicStoreContext context) : base(context) { }

        public List<AlbumArtist> GetAlbumArtistByArtist(int artistId)
        {
            try
            {
                var albumArtist = _entities.Where(x => x.ArtistId == artistId)
                        .ToList();

                return albumArtist;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
