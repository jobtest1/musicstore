﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MusicStore.Core.CustomEntities
{
    public class AlbumRating
    {
        public int Rating { get; set; }
        public int ReviewsCount { get; set; }
    }
}
