﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Genre : BaseEntity
    {
        public Genre()
        {
            AlbumGenre = new HashSet<AlbumGenre>();
        }

        public int GenreId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<AlbumGenre> AlbumGenre { get; set; }
    }
}
