﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class ArtistRepository : BaseRepository<Artist>, IArtistRepository
    {

        public ArtistRepository(MusicStoreContext context) : base(context) { }

        public Artist GetByName(string name)
        {
            return _entities.FirstOrDefault(x => x.Name == name);
        }
    }
}
