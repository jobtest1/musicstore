﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly MusicStoreContext _context;
        private readonly ISongRepository _songRepository;
        private readonly IAlbumRepository _albumRepository;
        private readonly IAlbumArtistRepository _albumArtistRepository;
        private readonly IArtistRepository _artistRepository;
        private readonly IAlbumProductorRepository _albumProductorRepository;
        private readonly IProductorRepository _productorRepository;
        private readonly IAlbumReviwerRepository _albumReviwerRepository;
        private readonly IRepository<Reviwer> _reviwerRepository;

        public UnitOfWork(MusicStoreContext context)
        {
            _context = context;
        }

        public ISongRepository SongRepository => _songRepository ?? new SongRepository(_context);

        public IAlbumRepository AlbumRepository => _albumRepository ?? new AlbumRepository(_context);

        public IAlbumArtistRepository AlbumArtistRepository => _albumArtistRepository ?? new AlbumArtistRepository(_context);

        public IArtistRepository ArtistRepository => _artistRepository ?? new ArtistRepository(_context);

        public IAlbumProductorRepository AlbumProductorRepository => _albumProductorRepository ?? new AlbumProductorRepository(_context);

        public IProductorRepository ProductorRepository => _productorRepository ?? new ProductorRepository(_context);

        public IAlbumReviwerRepository AlbumReviwerRepository => _albumReviwerRepository ?? new AlbumReviwerRepository(_context);

        public IRepository<Reviwer> ReviwerRepository => _reviwerRepository ?? new BaseRepository<Reviwer>(_context);

        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            try
            {
                _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
