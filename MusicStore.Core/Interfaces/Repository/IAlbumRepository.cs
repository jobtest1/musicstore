﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface IAlbumRepository : IRepository<Album>
    {
        Album GetByName(string name);
    }
}
