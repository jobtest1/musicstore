﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class ProductorRepository : BaseRepository<Productor>, IProductorRepository
    {
        public ProductorRepository(MusicStoreContext context) : base(context) { }

        public Productor GetByName(string name)
        {
            try
            {
                return _entities.FirstOrDefault(x => x.Name == name);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
