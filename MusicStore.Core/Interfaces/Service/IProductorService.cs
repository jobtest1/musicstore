﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface IProductorService
    {
        Task<List<Productor>> GetByAlbum(string albumName);
    }
}
