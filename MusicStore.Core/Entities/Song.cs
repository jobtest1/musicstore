﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Song : BaseEntity
    {
        public int SongId { get; set; }
        public int? AlbumId { get; set; }
        public string Name { get; set; }
        public int? TrackNumber { get; set; }
        public string Duration { get; set; }
        public int? Popularity { get; set; }
        public decimal? SongPrice { get; set; }

        public virtual Album Album { get; set; }
    }
}
