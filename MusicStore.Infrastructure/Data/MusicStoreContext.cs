﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using MusicStore.Core.Entities;

namespace MusicStore.Infrastructure.Data
{
    public partial class MusicStoreContext : DbContext
    {
        public MusicStoreContext()
        {
        }

        public MusicStoreContext(DbContextOptions<MusicStoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Album> Album { get; set; }
        public virtual DbSet<AlbumArtist> AlbumArtist { get; set; }
        public virtual DbSet<AlbumGenre> AlbumGenre { get; set; }
        public virtual DbSet<AlbumProductor> AlbumProductor { get; set; }
        public virtual DbSet<AlbumRecordCompany> AlbumRecordCompany { get; set; }
        public virtual DbSet<AlbumReviwer> AlbumReviwer { get; set; }
        public virtual DbSet<Artist> Artist { get; set; }
        public virtual DbSet<Genre> Genre { get; set; }
        public virtual DbSet<Productor> Productor { get; set; }
        public virtual DbSet<RecordCompany> RecordCompany { get; set; }
        public virtual DbSet<Reviwer> Reviwer { get; set; }
        public virtual DbSet<Song> Song { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Album>(entity =>
            {
                entity.Property(e => e.AlbumPrice).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.CoverImage)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MainReview)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordedDate).HasColumnType("datetime");

                entity.Property(e => e.ReleasedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<AlbumArtist>(entity =>
            {
                entity.HasOne(d => d.Album)
                    .WithMany(p => p.AlbumArtist)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_AlbumArtist_Album");

                entity.HasOne(d => d.Artist)
                    .WithMany(p => p.AlbumArtist)
                    .HasForeignKey(d => d.ArtistId)
                    .HasConstraintName("FK_SONGARTI_REFERENCE_ARTIST");
            });

            modelBuilder.Entity<AlbumGenre>(entity =>
            {
                entity.HasOne(d => d.Album)
                    .WithMany(p => p.AlbumGenre)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_ALBUMGEN_REFERENCE_ALBUM");

                entity.HasOne(d => d.Genre)
                    .WithMany(p => p.AlbumGenre)
                    .HasForeignKey(d => d.GenreId)
                    .HasConstraintName("FK_ALBUMGEN_REFERENCE_GENRE");
            });

            modelBuilder.Entity<AlbumProductor>(entity =>
            {
                entity.HasOne(d => d.Album)
                    .WithMany(p => p.AlbumProductor)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_ALBUMPRO_REFERENCE_ALBUM");

                entity.HasOne(d => d.Productor)
                    .WithMany(p => p.AlbumProductor)
                    .HasForeignKey(d => d.ProductorId)
                    .HasConstraintName("FK_ALBUMPRO_REFERENCE_PRODUCTO");
            });

            modelBuilder.Entity<AlbumRecordCompany>(entity =>
            {
                entity.HasKey(e => e.AlbumRecordCompany1)
                    .HasName("PK_ALBUMRECORDCOMPANY");

                entity.Property(e => e.AlbumRecordCompany1).HasColumnName("AlbumRecordCompany");

                entity.HasOne(d => d.Album)
                    .WithMany(p => p.AlbumRecordCompany)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_ALBUMREC_REFERENCE_ALBUM");

                entity.HasOne(d => d.RecordCompany)
                    .WithMany(p => p.AlbumRecordCompany)
                    .HasForeignKey(d => d.RecordCompanyId)
                    .HasConstraintName("FK_ALBUMREC_REFERENCE_RECORDCO");
            });

            modelBuilder.Entity<AlbumReviwer>(entity =>
            {
                entity.HasOne(d => d.Album)
                    .WithMany(p => p.AlbumReviwer)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_ALBUMREV_REFERENCE_ALBUM");

                entity.HasOne(d => d.Reviwer)
                    .WithMany(p => p.AlbumReviwer)
                    .HasForeignKey(d => d.ReviwerId)
                    .HasConstraintName("FK_ALBUMREV_REFERENCE_REVIWER");
            });

            modelBuilder.Entity<Artist>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Genre>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Productor>(entity =>
            {
                entity.Property(e => e.Lastname)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RecordCompany>(entity =>
            {
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Owner)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UrlPage)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Reviwer>(entity =>
            {
                entity.Property(e => e.Commentary)
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Song>(entity =>
            {
                entity.Property(e => e.Duration)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SongPrice).HasColumnType("decimal(10, 2)");

                entity.HasOne(d => d.Album)
                    .WithMany(p => p.Song)
                    .HasForeignKey(d => d.AlbumId)
                    .HasConstraintName("FK_SONG_REFERENCE_ALBUM");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
