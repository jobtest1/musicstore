﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicStore.UnitTest
{
    public class SongServiceFake : ISongService
    {
        private readonly FakeData _fakeData;

        public SongServiceFake()
        {
            _fakeData = new FakeData();
        }

        public async Task<Song> GetSong(int id)
        {
            await Task.Delay(5);
            return _fakeData.Songs.FirstOrDefault(x => x.SongId == id);
        }

        public List<Song> GetSongByAlbum(string albumName)
        {
            var album = _fakeData.Albums.FirstOrDefault(x => x.Name == albumName);

            if (album != null)
                return _fakeData.Songs.Where(x => x.AlbumId == album.AlbumId).ToList();
            else
                return null;
        }

        public IEnumerable<Song> GetSongs()
        {
            return _fakeData.Songs;
        }
    }
}
