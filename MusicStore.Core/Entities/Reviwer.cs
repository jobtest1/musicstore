﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class Reviwer : BaseEntity
    {
        public Reviwer()
        {
            AlbumReviwer = new HashSet<AlbumReviwer>();
        }

        public int ReviwerId { get; set; }
        public int? UserId { get; set; }
        public int? Rating { get; set; }
        public string Commentary { get; set; }

        public virtual ICollection<AlbumReviwer> AlbumReviwer { get; set; }
    }
}
