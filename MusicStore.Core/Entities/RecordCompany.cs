﻿using System;
using System.Collections.Generic;

namespace MusicStore.Core.Entities
{
    public partial class RecordCompany : BaseEntity
    {
        public RecordCompany()
        {
            AlbumRecordCompany = new HashSet<AlbumRecordCompany>();
        }

        public int RecordCompanyId { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string UrlPage { get; set; }

        public virtual ICollection<AlbumRecordCompany> AlbumRecordCompany { get; set; }
    }
}
