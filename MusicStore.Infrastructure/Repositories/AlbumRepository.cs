﻿using MusicStore.Core.Entities;
using MusicStore.Core.Interfaces;
using MusicStore.Infrastructure.Data;
using System;
using System.Linq;

namespace MusicStore.Infrastructure.Repositories
{
    public class AlbumRepository : BaseRepository<Album>, IAlbumRepository
    {
        public AlbumRepository(MusicStoreContext context) : base(context) { }

        public Album GetByName(string name)
        {
            try
            {
                return _entities.FirstOrDefault(x => x.Name == name);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
