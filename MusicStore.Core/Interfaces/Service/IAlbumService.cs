﻿using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public interface IAlbumService
    {
        Task<List<Album>> GetAlbumsByArtist(string artistName);
        Album GetByName(string name);
    }
}
