﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MusicStore.Api.Responses;
using MusicStore.Core.DTOs;
using MusicStore.Core.Interfaces;
using System;
using System.Collections.Generic;

namespace MusicStore.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        private readonly ISongService _songService;
        private readonly IAlbumService _albumService;
        private readonly IProductorService _productorService;
        private readonly IReviwerService _reviwerService;
        private readonly ILoggerManager _logger;
        private readonly IMapper _mapper;

        public MusicController(ISongService songService, 
                               IAlbumService albumService, 
                               IProductorService productorService, 
                               IReviwerService reviwerService,
                               ILoggerManager logger,
                               IMapper mapper)
        {
            _songService = songService;
            _albumService = albumService;
            _productorService = productorService;
            _reviwerService = reviwerService;
            _logger = logger;
            _mapper = mapper;
        }

        /// <summary>
        /// Get songs by album name
        /// </summary>
        /// <param name="albumName"></param>
        /// <returns></returns>
        [HttpGet("{albumName}")]
        public IActionResult GetSongsByAlbum(string albumName)
        {
            try
            {
                _logger.LogInfo($"Endpoint {nameof(GetSongsByAlbum)} started");
                var songs = _songService.GetSongByAlbum(albumName);

                if(songs != null)
                {
                    var songsDto = _mapper.Map<List<SongDto>>(songs);
                    var response = new ApiResponse<List<SongDto>>(songsDto);
                    return Ok(response);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception {ex} at Endpoint {nameof(GetSongsByAlbum)}");
                return BadRequest();
            }
        }

        /// <summary>
        /// Get albums by artist name
        /// </summary>
        /// <param name="artistName"></param>
        /// <returns></returns>
        [HttpGet("{artistName}")]
        public IActionResult GetAlbumsByArtist(string artistName)
        {
            try
            {
                _logger.LogInfo($"Endpoint {nameof(GetAlbumsByArtist)} started");
                var albums = _albumService.GetAlbumsByArtist(artistName);

                if (albums != null)
                {
                    var albumsDto = _mapper.Map<List<AlbumDto>>(albums);
                    var response = new ApiResponse<List<AlbumDto>>(albumsDto);
                    return Ok(response);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception {ex} at Endpoint {nameof(GetAlbumsByArtist)}");
                return BadRequest();
            }
        }

        /// <summary>
        /// Get Album by name
        /// </summary>
        /// <param name="albumName"></param>
        /// <returns></returns>
        [HttpGet("{albumName}")]
        public IActionResult GetAlbumByName(string albumName)
        {
            try
            {
                _logger.LogInfo($"Endpoint {nameof(GetAlbumByName)} started");
                var album = _albumService.GetByName(albumName);

                if (album != null)
                {
                    var albumDto = _mapper.Map<AlbumDto>(album);
                    var response = new ApiResponse<AlbumDto>(albumDto);
                    return Ok(response);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception {ex} at Endpoint {nameof(GetAlbumByName)}");
                return BadRequest();
            }
        }

        /// <summary>
        /// Get productors by albumName
        /// </summary>
        /// <param name="albumName"></param>
        /// <returns></returns>
        [HttpGet("{albumName}")]
        public IActionResult GetProductorsByAlbum(string albumName)
        {
            try
            {
                _logger.LogInfo($"Endpoint {nameof(GetProductorsByAlbum)} started");
                var productors = _productorService.GetByAlbum(albumName);

                if (productors != null)
                {
                    var productorsDto = _mapper.Map<List<ProductorDto>>(productors);
                    var response = new ApiResponse<List<ProductorDto>>(productorsDto);
                    return Ok(response);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception {ex} at Endpoint {nameof(GetProductorsByAlbum)}");
                return BadRequest();
            }
        }

        /// <summary>
        /// Get Album rating by album name
        /// </summary>
        /// <param name="albumName"></param>
        /// <returns></returns>
        [HttpGet("{albumName}")]
        public IActionResult GetAlbumRating(string albumName)
        {
            try
            {
                _logger.LogInfo($"Endpoint {nameof(GetAlbumRating)} started");
                var albumRating = _reviwerService.GetAlbumRating(albumName);

                if (albumRating != null)
                {
                    var albumRatingDto = _mapper.Map<AlbumRatingDto>(albumRating);
                    var response = new ApiResponse<AlbumRatingDto>(albumRatingDto);
                    return Ok(response);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception {ex} at Endpoint {nameof(GetAlbumRating)}");
                return BadRequest();
            }
        }

    }
}
