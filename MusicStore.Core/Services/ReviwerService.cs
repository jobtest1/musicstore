﻿using MusicStore.Core.CustomEntities;
using MusicStore.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MusicStore.Core.Interfaces
{
    public class ReviwerService : IReviwerService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ReviwerService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<AlbumRating> GetAlbumRating(string albumName)
        {
            try
            {
                AlbumRating rating = new AlbumRating();
                List<Reviwer> reviwers = new List<Reviwer>();
                int reviwersCount = 0;
                int averageRating = 0;
                var album = _unitOfWork.AlbumRepository.GetByName(albumName);

                if(album != null)
                {
                    var albumReviwers = _unitOfWork.AlbumReviwerRepository.GetReviewsByAlbum(album.AlbumId);

                    foreach (var item in albumReviwers)
                    {
                        var reviwer = await _unitOfWork.ReviwerRepository.GetById((int)item.ReviwerId);
                        reviwers.Add(reviwer);
                        averageRating += (int)reviwer.Rating;
                    }
                    reviwersCount = albumReviwers.Count;
                    averageRating = averageRating / reviwersCount;

                    return new AlbumRating() { Rating = averageRating, ReviewsCount = reviwersCount };
                }
                else
                    return null;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
